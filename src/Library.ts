import { ILibraryRepository } from "./ILibraryRepository";
import { Book } from "./Book";
import { injectable } from 'inversify';

@injectable()
export class Library implements ILibraryRepository {

  private bestSellingBooksList: Book[] = [
    new Book(1, 'Don Quixote', 'Miguel de Cervantes', 500, 1605),
    new Book(2, 'A Tale of Two Cities', 'Charles Dickens', 200, 1859),
    new Book(3, 'The Lord of the Rings', 'J.R.R. Tolkien', 150, 1954),
    new Book(4, 'The Little Prince', 'Antoine de Saint-Exupery', 142, 1943),
    new Book(5, 'Harry Potter and the Sorcerer’s Stone', 'J.K. Rowling', 107, 1997),
  ];

  getBooks(): Book[] {
    return this.bestSellingBooksList;
  }

  getBookById(id: number): Book {
    return this.bestSellingBooksList.find(book => book.id === id);
  }

  addBook(book: Book): number {
    return this.bestSellingBooksList.push(book);
  }

  editBook(id: number, book: Book): Book {
    const bookIndex = this.bestSellingBooksList.findIndex(book => book.id === id);

    this.bestSellingBooksList[bookIndex].title = book.title;
    this.bestSellingBooksList[bookIndex].author = book.author;
    this.bestSellingBooksList[bookIndex].soldCopies = book.soldCopies;
    this.bestSellingBooksList[bookIndex].year = book.year;

    return this.bestSellingBooksList[bookIndex];
  }

  deleteBook(id: number): Book {
    const bookIndex = this.bestSellingBooksList.findIndex(book => book.id === id);

    return bookIndex > -1 ? this.bestSellingBooksList.splice(bookIndex, 1)[0] : null;
  }
}