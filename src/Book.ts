export class Book {

  public id: number;
  public title: string;
  public author: string;
  public soldCopies: number;
  public year: number;

  constructor(
    id: number,
    title: string,
    author: string,
    soldCopies: number,
    year: number,
  )
  {
    this.id = id;
    this.title = title;
    this.author = author;
    this.soldCopies = soldCopies;
    this.year = year;
  }
}
