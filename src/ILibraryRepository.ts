import { Book } from './Book';

export interface ILibraryRepository {
  getBooks(): Book[];
  getBookById(id: number): Book;
  addBook(book: Book): number;
  editBook(id: number, book: Book): Book;
  deleteBook(id: number): Book;
}