import { ILibraryRepository } from "./ILibraryRepository";
import { Book } from "./Book";

export class LibraryService {
  private repository: ILibraryRepository;

  constructor(repository: ILibraryRepository) {
    this.repository = repository;
  }

  getBooks(): Book[] {
    return this.repository.getBooks();
  }

  getBookById(id: number): Book {
    return this.repository.getBookById(id);
  }

  addBook(book: Book): number {
    return this.repository.addBook(book);
  }

  editBook(id: number, book: Book): Book {
    return this.repository.editBook(id, book);
  }

  deleteBook(id: number): Book {
    return this.repository.deleteBook(id);
  }
}