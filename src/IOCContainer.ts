import { ILibraryRepository } from "./ILibraryRepository";
import { Container } from "inversify";
import "reflect-metadata";

import LIBRARY_SERVICE from "./constants";
import { Library } from './Library';

let container = new Container();

container.bind<ILibraryRepository>(LIBRARY_SERVICE.ILibraryRepository).to(Library);

export default container;