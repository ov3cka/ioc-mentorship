import LIBRARY_SERVICE from "./constants";
import { ILibraryRepository } from "./ILibraryRepository";
import { LibraryService } from "./LibraryService";
import container from "./IOCContainer";

const libraryRepository = container.get<ILibraryRepository>(LIBRARY_SERVICE.ILibraryRepository);
const libraryService = new LibraryService(libraryRepository);

console.log(libraryService.getBooks());